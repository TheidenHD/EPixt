FROM python:3.7.6 AS build
WORKDIR /dep
RUN git config --global http.sslverify false
RUN git clone https://git.kontron-electronics.de/sw/ked/raspberry-pi/pixtend-v2/pplv2.git
WORKDIR /dep/pplv2
RUN pip3 install setuptools
RUN python3 setup.py sdist
RUN pip3 install ./dist/*.tar.gz
WORKDIR /EPixt
RUN pip3 install emmi pyyaml && rm -rf /root/.cache
COPY . /EPixt

FROM build AS test
RUN pip3 install pytest pytest-cov pytest-asyncio caproto && rm -rf /root/.cache
ENTRYPOINT [ "pytest", "--cov=src" ]

FROM build AS final
EXPOSE 5064/tcp
EXPOSE 5064/udp
EXPOSE 5065/tcp
EXPOSE 5065/udp
ENTRYPOINT [ "python3", "src/EPixt/EPixt.py" ]
