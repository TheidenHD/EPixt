# EPixt

[![Pipeline Status](../../../badges/main/pipeline.svg)](../../commits)
[![Coverage Report](../../../badges/main/coverage.svg)](../../commits)
[![Latest Release](../../badges/release.svg)](../../releases)

EPixt is a tool to make an PiXtend V2-S/L Controler accessible via EPICS.

## Getting started
Docker Compose is required to run EPixt.

Download the Docker-Compose-Files from the [current release](../../releases) (under "Assets/Other"), it contains 2 files:
- .env
- docker-compose.yml

Change the path in .env so it points to a valid cofig file.

Original:
```dotenv
HOST_CONFIG_PATH=/path/to/host/config/file
```

Example:
```dotenv
HOST_CONFIG_PATH=/home/demo/Documents/config.yaml
```

Start the Container with: 
```shell
docker compose up
```

## Create a cofig file
EPixt uses YAML files for configuration.

Example:
```yml
type: l
demo: True
prefix: Demo
global:
    gpio0_ctrl: Signal
    pullups: True
    gpio0: True
channels:
    relay0:
        label: Test1
    digital_out1:
        label: Test2
    digital_in1:
        label: Test3
    gpio0:
        label: Test4
    analog_out2:
        label: Test5
    analog_in2_raw:
        label: Test6
```

### type(Required)
Defines which hardware version is used:
- ```s```: PiXtend V2-S
- ```l```: PiXtend V2-L

### demo(Optional)
Defines if the hardware should be simulated.
- ```True/False(Default: False)```

### prefix(Optional, Recommended)
Defines the name of the EPICS-IOC.
If no prefix is given, it will use the hostname of the Device. If it runs inside of a Container the hostname of the Container will be used.

### global(Required)
Defines a list of properties and values that have to be set for the PiXtend before the EPICS-IOC starts.

Config:
```yml
global:
    propertie: value
```

Execution:
```py
PiXtend.propertie = value
```

Example, pullup on GPIO 0:
```yml
global:
    gpio0_ctrl: Signal  # Make the GPIO an input
    pullups: True       # Enable the pullups
    gpio0: True         # Writing True to the GPIO (as if it was an output) activates
```

For a full list of usable properties please read the [Documentation of the PiXtend driver.](https://git.kontron-electronics.de/sw/ked/raspberry-pi/pixtend-v2/pplv2)

A few Keywords have bean defined to be used instead of the value:
- ```Signal```
- ```Actor```
- ```Input```
- ```Output```
- ```DHT11```
- ```DHT22```

### channels(Required)
Defines a list of channels that will be accessible with EPICS.

Supported are:
- ```relay{n}```
- ```gpio{n}```
- ```digital_{out/in}{n}```
- ```analog_{out/in}{n}```
- ```analog_{out/in}{n}_raw```

Example:
```yml
channels:
    relay0:
        label: Test1
```    

Each channel needs a unique label.

## Notes
A local GitLab Runner was used, a new one may be required in the future.