from util import parseDcl

class DemoPixtend:
    '''
    Simulates the Hardware.

    Args:
        type (str): Specifies which hardware version to simulate.

    Attributes:
        parameters (dict): Specifies the number of pins of each type.
        values (dict): Contains the values for all the pins.
    '''

    def __init__(self, type):
        self.values = {}
        if type == 's':
            self.parameters = {
                'digital': [4, 8],
                'relay': [4, 4],
                'gpio': [4, 4],
                'analog_raw': [2, 2]
            }
        else:
            self.parameters = {
                'digital': [12, 16],
                'relay': [4, 4],
                'gpio': [4, 4],
                'analog_raw': [2, 6]
            }
            
    def __getattr__(self, attr):
        ptype, pdir, pid = parseDcl(attr)
        if ptype == 'gpio' and pdir == 'ctrl' and self.parameters[ptype][0] > int(pid):
            return self.values.get(ptype + '_' + pdir + pid, 0)
        elif ptype in self.parameters and self.parameters[ptype][1] > int(pid):
            return self.values.get(ptype + pid, 0) 
        elif ptype == 'analog' and self.parameters['analog_raw'][1] > int(pid):
            return self.values.get(ptype + '_raw' + pid, 0) * (10.0 / 1024)

    def __setattr__(self, attr, value):
        if attr == 'values' or attr == 'parameters':
            object.__setattr__(self, attr, value)
        else:
            ptype, pdir, pid = parseDcl(attr)
            if ptype == 'gpio' and pdir == 'ctrl' and self.parameters[ptype][0] > int(pid):
                self.values[ptype + '_' + pdir + pid]  = value
            elif ptype in self.parameters and self.parameters[ptype][0] > int(pid):
                self.values[ptype + pid]  = value
            elif ptype == 'analog' and self.parameters['analog_raw'][0] > int(pid):
                self.values[ptype + '_raw' + pid]  = int(value * (1024 / 10.0))

    def close(self):
        print('Closed')