import os
import socket
import sys
import yaml
from emmi.app import IocApplication
from Pixtend import Pixtend
from util import parseDcl

import logging

'''
Eigentlich ist der Plan folgender:
  - Pixtend hat Kanäle (GPIO 1, 2, 3... Analog in 1, 2, 3...)
  - ...der Mensch hat aber "Funktionen" ("Pumpe", "Temperatur", "Ventil"...)
    (Device Channel Label?)
  - 
'''

def ptype2validator(kind, type):
    '''
    Gets the Validator vor a specific Channel
    
    Args:
        kind (str): Specifies wat kiend of variable
        type (str): Specifies if it is a Actor ore Signal(Input ore Output)

    Returns:
        dict: Validator configuration
    '''
    if kind == 'switch':
        return {
            'actor':{
                'values':{ 
                    'ON': True,
                    True: True, 
                    1: True,
                    'OFF': False, 
                    False: False, 
                    0: False
                }
            },
            'signal':{
                'values':{ 
                    'ON': 'ON',
                    True: 'ON', 
                    1: 'ON',
                    'OFF': 'OFF', 
                    False: 'OFF', 
                    0: 'OFF'
                }
            }
        }[type]
    else:
        return {
            'analog': {},
            'integer': {}
        }[kind]

def port2eapi(name, channelSpec, pixtend):
    ''' Creates an EMMI Export-API dictionary from a EPixt channel-publish dict item
    
    Args:
        name (str): Function Name that will be exported. Supported are:
            relay{n}
            gpio{n}
            digital_{out/in}{n}
            analog_{out/in}{n}
            analog_{out/in}{n}_raw

        channelSpec (dict): EPixt channel-publish dict.

        pixtend (Rixtend): Reference to hardware.

    Returns:
        dict: Full EMMI Export-API dictionary.
    '''

    #schema = Schema()
    #data = schema.validate(portSpec)

    ptype, pdir, pid = parseDcl(name) # format: "{ptype}_{pdir}{pid}_{ptype_suffix}"

    ptype2record = {
        'in': 'signal',
        'out': 'actor',
        'relay': 'actor'
    }
    
    ptype2kind = {
        'digital': 'switch',
        'relay': 'switch',
        'gpio': 'switch',
        'analog': 'analog',
        'analog_raw': 'integer'
    }

    epicSpec = {}

    epicSpec['suffix'] = channelSpec['label']
    epicSpec['name'] = name
    epicSpec['kind'] = ptype2kind[ptype]

    if ptype == 'gpio':
        types = {
                0: 'signal',
                1: 'actor',
                2: 'signal',
                3: 'signal'}
        type = types[getattr(pixtend.p, name + '_ctrl')]
    else:
        type = ptype2record[pdir]
    
    print(ptype)
    print(ptype2kind[ptype])
    print(type)
    epicSpec['validator'] = ptype2validator(ptype2kind[ptype], type)

    return {
        'recordType': type,
        type: epicSpec
    }

def main(argv):
    with open(argv[0] if len(argv) > 0 else os.path.dirname(os.path.abspath(__file__)) + '/config.yaml', 'r') as file:
        data = yaml.safe_load(file)
    p = Pixtend(data)

    app = IocApplication(prefix = data['prefix'] if 'prefix' in data else socket.gethostname(), setupIoc=True)

    for k,v in data["global"].items():
        p.setupGlobalSettings(k, v)

    for k,v in data["channels"].items():
        app.exportObject(p.p, port2eapi(k, v, p))
    
    app.runIoc()

if __name__ == "__main__":
    main(sys.argv[1:])