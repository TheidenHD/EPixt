import re
import sys
import time

class Pixtend:
    '''
    Abstraction-Layer for the Hardware.

    Args:
        settings (dict): Dictionary which contains the Hardware specifications.

    Attributes:
        demo (bool): Determents if the Hardware is simulated.
    '''

    def __init__(self, settings):
        self.demo = settings.get('demo', False)
        if self.demo and (settings['type'] == 's' or settings['type'] == 'l'):
            from DemoPixtend import DemoPixtend
            self.p = DemoPixtend(settings['type'])
        elif settings['type'] == 's':
            from pixtendv2s import PiXtendV2S
            self.p = PiXtendV2S()
        elif settings['type'] == 'l':
            from pixtendv2l import PiXtendV2L
            self.p = PiXtendV2L()
        else:
            sys.exit('Invalid device type')

    def __del__(self):
        if hasattr(self, 'p'):
            time.sleep(0.5)
            self.p.close()
            time.sleep(0.25)

    def setupGlobalSettings(self, name, spec):
        '''
        Configures the Pixtend.

        Args:
            kind (str): Setting that will be set.
            spec (str): Value that the setting will be set to.
        '''
        type = {
            'Signal': 0,
            'Actor': 1,
            'Input': 0,
            'Output': 1,
            'DHT11': 2,
            'DHT22': 3}
            
        if hasattr(self.p, name):
            setattr(self.p, name, type.get(spec, spec))