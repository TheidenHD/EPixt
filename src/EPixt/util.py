import re

'''
Library of miscellaneous functions that either didn’t fit into a different one 
or needed to be used by multiple one’s.
'''

def parseDcl(name):
    '''
    Parses the device channel label (DCL) to extract Pixtend port specs.

    Args:
        name (str): String with the strukture `{ptype}_{pdir}{pid}_{ptype_suffix}`.
            pdir and ptype_suffix are Optional `{ptype}{pid}`.

    Returns: 
        tuple: Tuple of three strings with the strukture `{ptype}_{ptype_suffix} {pdir} {pid}`.
            If pdir does not exist, then the first and second string are identical `{ptype} {ptype} {pid}`.
            If a string cannot be converted, it is returned as is three times.
    '''
    spec = re.sub(r"([^_]+)(\D*)(\d+)(\D*)", r"\1\4\2_\3", name)
    spec2 = spec.rsplit('_', 2)
    if len(spec2) < 2:
        return name, name, name
    else:
        return spec2[0], spec2[-2], spec[-1]