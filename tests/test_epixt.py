import pytest
import yaml
import time
import asyncio
from emmi.app import IocApplication
import EPixt
from EPixt import Pixtend
from util import parseDcl

def test_ptype2validator():

    assert EPixt.ptype2validator('analog', 'actor') == {}

    assert EPixt.ptype2validator('integer', 'actor') == {}

    assert EPixt.ptype2validator('switch', 'actor')['values']['ON'] == True

    assert EPixt.ptype2validator('switch', 'signal')['values'][False] == 'OFF'
    
def test_parseDcl():

    assert parseDcl('digital_out0') == ('digital', 'out', '0')

    assert parseDcl('relay6') == ('relay', 'relay', '6')

    assert parseDcl('analog_in3_raw') == ('analog_raw', 'in', '3')

    assert parseDcl('demo') == ('demo', 'demo', 'demo')

def test_demopixtend():
    s = Pixtend({'type': 's', 'demo': True})
    l = Pixtend({'type': 'l', 'demo': True})
    s.setupGlobalSettings('gpio0_ctrl', 'Signal')
    assert s.p.gpio0_ctrl == 0
    s.p.gpio0 = 1
    assert s.p.gpio0 == 1
    s.p.analog_in1 = 1.0
    assert s.p.analog_out1_raw == 102

    with pytest.raises(SystemExit) as pytest_wrapped_e:
            Pixtend({'type': 'o'})
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 'Invalid device type'

@pytest.mark.asyncio
async def test_app_async_defaultioc():

    with open('./tests/demo.yaml', 'r') as file:
        data = yaml.safe_load(file)
    p = Pixtend(data)

    app = IocApplication(prefix = data['prefix'], setupIoc=True)
    
    for k,v in data["global"].items():
        p.setupGlobalSettings(k, v)

    for k,v in data["channels"].items():
        app.exportObject(p.p, EPixt.port2eapi(k, v, p))
    
    app.startIoc()

    from caproto.sync import client

    tstart = time.time()
    while time.time()-tstart < 3.0:
        print (client.read(data['prefix']+":heartbeat").data)
        await asyncio.sleep(0.5)

    app.stopIoc()
